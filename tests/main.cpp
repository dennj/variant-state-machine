#define CATCH_CONFIG_MAIN

#include "pch.h"
#include "StateA.h"
#include "StateB.h"

namespace UnitTestSM {

TEST_CASE("StateA_test")
{
  SECTION("Change was too late")
  {
    StateA test_state;
    REQUIRE_FALSE(std::holds_alternative<std::monostate>(test_state.Run()));
  }
}

TEST_CASE("StateB_test")
{
  SECTION("Change was too early or too late")
  {
    StateB test_state;
    for (int i = 0; i < 2; ++i)
    {
      REQUIRE(std::holds_alternative<std::monostate>(test_state.Run()));
    }
    REQUIRE_FALSE(std::holds_alternative<std::monostate>(test_state.Run()));
  }
}

}
