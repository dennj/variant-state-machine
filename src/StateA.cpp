//	Dennj Osele
//	12 Aug 2018

#include "pch.h"
#include "StateMachine.h"
#include "StateB.h"

StateA::NextState StateA::Run() {
  std::cout << "Run state A \n";
  return StateB();
}

void StateA::OnLeaving() { std::cout << "Leaving A \n"; }

void StateA::OnEntrance() { std::cout << "Entrance A \n"; }
