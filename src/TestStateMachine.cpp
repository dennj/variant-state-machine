//	Dennj Osele
//	12 Aug 2018

#include "pch.h"
#include "StateMachine.h"

int main() {
  StateMachine state;
  for (int i = 0; i < 10; ++i)
  {
    state.Run();
  }
  return 0;
}
