//	Dennj Osele
//	12 Aug 2018

#include "pch.h"
#include "StateMachine.h"

void StateMachine::Run() {
  visit(overloaded
      {
          [this](auto& state) noexcept {
              auto next_state = state.Run();
              if (!std::holds_alternative<std::monostate>(next_state))	// check if it has a new state to transit
              {
                  TransitToState(next_state);
              }
          }
      }, current_state);
}
