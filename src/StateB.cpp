//	Dennj Osele
//	12 Aug 2018

#include "pch.h"
#include "StateA.h"
#include "StateMachine.h"

StateB::NextState StateB::Run() {
  std::cout << "Run state B \n";

  if (--counter == 0)
    return StateA();

  return {};
}

void StateB::OnLeaving() { std::cout << "Leaving B \n"; }

void StateB::OnEntrance() { std::cout << "Entrance in B \n"; }
