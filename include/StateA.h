//	Dennj Osele
//	12 Aug 2018

#pragma once

class StateA
{
public:
  using NextState = std::variant<std::monostate, class StateB>;
  NextState Run();

  void OnLeaving();
  void OnEntrance();
};
