//	Dennj Osele
//	12 Aug 2018

#pragma once

class StateB
{
public:
  using NextState = std::variant<std::monostate, class StateA>;
  NextState Run();

  void OnLeaving();
  void OnEntrance();

private:
  int counter = 3;
};
