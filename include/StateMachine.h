//	Dennj Osele
//	12 Aug 2018

#pragma once

#include "StateA.h"
#include "StateB.h"

// Standard helper templates, used for enable_if mechanism.
template < template <typename...> class Template, typename T >
struct is_instantiation_of : std::false_type {};
template < template <typename...> class Template, typename... Args >
struct is_instantiation_of< Template, Template<Args...> > : std::true_type {};
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;

class StateMachine {
public:
  StateMachine() = default;

  void Run();

  // Moving values from new_state variant to current_state variant.
  template<class T>
  std::enable_if_t< is_instantiation_of<std::variant, std::remove_reference_t<T>>::value >
  TransitToState(T&& new_state)
  {
    std::visit( overloaded {
                   [this]([[maybe_unused]] std::monostate var){}, // place holder for no value
                   [this](auto&& var){
                     std::visit([](auto &current_state) noexcept { current_state.OnLeaving(); }, current_state);
                     current_state = std::forward<decltype(var)>(var); // current_state.emplace<T>(std::forward<T>(var));
                     std::visit([](auto &current_state) noexcept { current_state.OnEntrance(); }, current_state);
                   }
               }, new_state);
  }

private:
  using State = std::variant<StateA, StateB>;
  State current_state{ StateA() };
};
